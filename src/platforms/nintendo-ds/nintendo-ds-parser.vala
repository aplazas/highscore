// This file is part of GNOME Games. License: GPL-3.0+.

public class Games.NintendoDsParser : GameParser {
	private Icon icon;

	public NintendoDsParser (Platform platform, Uri uri) {
		base (platform, uri);
	}

	public override void parse () throws Error {
		icon = new NintendoDsIcon (uri);
	}

	public override Icon? get_icon () {
		return icon;
	}
}
