// This file is part of GNOME Games. License: GPL-3.0+.

public class Games.SegaSaturnParser : GameParser {
	private const string CUE_MIME_TYPE = "application/x-cue";
	private const string SEGA_SATURN_MIME_TYPE = "application/x-saturn-rom";

	private string uid;

	public SegaSaturnParser (Platform platform, Uri uri) {
		base (platform, uri);
	}

	public override void parse () throws Error {
		var file = uri.to_file ();
		var file_info = file.query_info (FileAttribute.STANDARD_CONTENT_TYPE, FileQueryInfoFlags.NONE);
		var mime_type = file_info.get_content_type ();

		File bin_file;
		switch (mime_type) {
		case CUE_MIME_TYPE:
			var cue = new CueSheet (file);
			bin_file = get_binary_file (cue);

			break;
		case SEGA_SATURN_MIME_TYPE:
			bin_file = file;

			break;
		default:
			throw new SegaSaturnError.INVALID_FILE_TYPE ("Invalid file type: expected %s or %s but got %s for file %s.", CUE_MIME_TYPE, SEGA_SATURN_MIME_TYPE, mime_type, uri.to_string ());
		}

		var header = new SegaSaturnHeader (bin_file);
		header.check_validity ();

		var prefix = platform.get_uid_prefix ();
		var product_number = header.get_product_number ();
		var areas = header.get_areas ();

		uid = @"$prefix-$product_number-$areas".down ();
	}

	public override string get_uid () {
		return uid;
	}

	private static File get_binary_file (CueSheet cue) throws Error {
		if (cue.tracks_number == 0)
			throw new SegaSaturnError.INVALID_CUE_SHEET ("The file “%s” doesn’t have a track.", cue.file.get_uri ());

		var track = cue.get_track (0);
		var file = track.file;

		if (file.file_format != CueSheetFileFormat.BINARY && file.file_format != CueSheetFileFormat.UNKNOWN)
			throw new SegaSaturnError.INVALID_CUE_SHEET ("The file “%s” doesn’t have a valid binary file format.", cue.file.get_uri ());

		if (!track.track_mode.is_mode1 ())
			throw new SegaSaturnError.INVALID_CUE_SHEET ("The file “%s” doesn’t have a valid track mode for track %d.", cue.file.get_uri (), track.track_number);

		var file_info = file.file.query_info ("*", FileQueryInfoFlags.NONE);
		if (file_info.get_content_type () != SEGA_SATURN_MIME_TYPE)
			throw new SegaSaturnError.INVALID_FILE_TYPE ("The file “%s” doesn’t have a valid Sega Saturn binary file.", cue.file.get_uri ());

		return file.file;
	}
}
