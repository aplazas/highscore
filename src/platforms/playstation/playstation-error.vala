// This file is part of GNOME Games. License: GPL-3.0+.

errordomain Games.PlayStationError {
	INVALID_HEADER,
	INVALID_FILE_TYPE,
	INVALID_CUE_SHEET,
}
