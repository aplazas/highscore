// This file is part of GNOME Games. License: GPL-3.0+.

// TODO support unknown games (not in DB)
public class Games.PlayStationParser : GameParser {
	private const string CUE_MIME_TYPE = "application/x-cue";
	private const string ICON_NAME = "media-optical-symbolic";
	private const string GAMEINFO = "resource:///org/gnome/Games/platforms/playstation/playstation.gameinfo.xml";

	private static GameinfoDoc gameinfo;
	private string uid;
	private string disc_id;
	private string disc_set_id;

	public PlayStationParser (Platform platform, Uri uri) {
		base (platform, uri);
	}

	public override void parse () throws Error {
		var file = uri.to_file ();
		var file_info = file.query_info (FileAttribute.STANDARD_CONTENT_TYPE, FileQueryInfoFlags.NONE);
		var mime_type = file_info.get_content_type ();

		File bin_file;
		switch (mime_type) {
		case CUE_MIME_TYPE:
			var cue = new CueSheet (file);
			if (cue.tracks_number == 0)
				throw new PlayStationError.INVALID_CUE_SHEET ("The file “%s” doesn’t have a track.", cue.file.get_uri ());

			var track = cue.get_track (0);
			if (track.track_mode != CueSheetTrackMode.MODE1_2352 &&
			    track.track_mode != CueSheetTrackMode.MODE2_2352)
				throw new PlayStationError.INVALID_CUE_SHEET ("The file “%s” doesn’t have a valid track mode for track %d.", cue.file.get_uri (), track.track_number);

			bin_file = track.file.file;

			break;
		// TODO Add support for binary files.
		default:
			throw new PlayStationError.INVALID_FILE_TYPE ("Invalid file type: expected %s but got %s for file %s.", CUE_MIME_TYPE, mime_type, uri.to_string ());
		}

		var header = new PlayStationHeader (bin_file);
		header.check_validity ();

		disc_id = header.disc_id;

		var gameinfo = get_gameinfo ();
		disc_set_id = gameinfo.get_disc_set_id_for_disc_id (disc_id);

		var prefix = platform.get_uid_prefix ();
		uid = @"$prefix-$disc_set_id".down ();
	}

	public override string get_uid () {
		return uid;
	}

	public override string? get_media_id () {
		return disc_id;
	}

	public override string? get_media_set_id () {
		return disc_set_id;
	}

	public override MediaSet? create_media_set () throws Error {
		var new_medias = new HashTable<string, Media> (str_hash, str_equal);
		Media[] new_medias_array = {};
		var new_disc_ids = gameinfo.get_disc_set_ids_for_disc_id (disc_id);
		foreach (var new_disc_id in new_disc_ids) {
			string title;
			try {
				title = gameinfo.get_disc_title_for_disc_id (new_disc_id);
			}
			catch (Error e) {
				var index = gameinfo.get_disc_set_index_for_disc_id (new_disc_id);

				title = _("Disc %d").printf (index + 1);
			}

			var media = new Media (new_disc_id, title);
			new_medias_array += media;
			new_medias[new_disc_id] = media;
		}

		var media = new_medias.lookup (disc_id);
		media.add_uri (uri);

		var media_set = new MediaSet (disc_set_id, gameinfo.get_game_title_for_disc_id (disc_set_id));
		foreach (var game_media in new_medias_array)
			media_set.add_media (game_media);
		media_set.icon_name = ICON_NAME;

		return media_set;
	}

	private static GameinfoDoc get_gameinfo () throws Error {
		if (gameinfo != null)
			return gameinfo;

		var file = File.new_for_uri (GAMEINFO);
		var input_stream = file.read ();

		input_stream.seek (0, SeekType.END);
		var length = input_stream.tell ();
		input_stream.seek (0, SeekType.SET);

		var buffer = new uint8[length];
		size_t size = 0;

		input_stream.read_all (buffer, out size);

		gameinfo = new GameinfoDoc.from_data (buffer);

		return gameinfo;
	}
}
