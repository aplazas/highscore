// This file is part of GNOME Games. License: GPL-3.0+.

public class Games.WiiParser : GameParser {
	private string uid;

	public WiiParser (Platform platform, Uri uri) {
		base (platform, uri);
	}

	public override void parse () throws Error {
		var file = uri.to_file ();
		var header = new WiiHeader (file);
		header.check_validity ();

		var prefix = platform.get_uid_prefix ();
		var game_id = header.get_game_id ();

		uid = @"$prefix-$game_id".down ();
	}

	public override string get_uid () {
		return uid;
	}
}
