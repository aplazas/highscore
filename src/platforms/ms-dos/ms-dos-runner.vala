// This file is part of GNOME Games. License: GPL-3.0+.

private class Games.MsDosRunner : Runner {
	public MsDosRunner (Game game, CoreSource source) {
		base (game, source);
	}

	construct {
		input_capabilities = new InputCapabilities (true, false);
	}
}
