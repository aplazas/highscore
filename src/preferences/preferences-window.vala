// This file is part of GNOME Games. License: GPL-3.0+.

[GtkTemplate (ui = "/org/gnome/Games/preferences/preferences-window.ui")]
private class Games.PreferencesWindow : Hdy.PreferencesWindow {
	private Binding swipe_back_binding;

	public void open_subpage (PreferencesSubpage subpage) {
		swipe_back_binding = subpage.bind_property (
			"allow-back", this, "can-swipe-back", BindingFlags.SYNC_CREATE);

		subpage.back.connect (() => {
			swipe_back_binding.unbind ();
			close_subpage ();
		});

		present_subpage (subpage);
	}
}
