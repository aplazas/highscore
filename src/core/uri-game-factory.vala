// This file is part of GNOME Games. License: GPL-3.0+.

public class Games.UriGameFactory : Object {
	private Platform platform;
	private HashTable<Uri, Game> game_for_uri;
	private unowned GameCallback game_added_callback;

	private HashTable<string, Media> medias;
	private HashTable<string, Game> game_for_media_set;
	private GenericSet<Game> games;

	public UriGameFactory (Platform platform) {
		this.platform = platform;

		game_for_uri = new HashTable<Uri, Game> (Uri.hash, Uri.equal);
		medias = new HashTable<string, Media> (str_hash, str_equal);
		game_for_media_set = new HashTable<string, Game> (str_hash, str_equal);
		games = new GenericSet<Game> (direct_hash, direct_equal);
	}

	public string[] get_mime_types () {
		return platform.get_mime_types ();
	}

	public void add_uri (Uri uri) {
		try {
			add_uri_with_error (uri);
		}
		catch (Error e) {
			debug (e.message);
		}
	}

	private void add_uri_with_error (Uri uri) throws Error {
		if (game_for_uri.contains (uri))
			return;

		var parser = Object.new (
			platform.parser_type,
			platform: platform,
			uri: uri
		) as GameParser;

		parser.parse ();

		var media_id = parser.get_media_id ();
		var media_set_id = parser.get_media_set_id ();

		if (media_id == null || media_set_id == null) {
			var game = create_game (parser);
			game_for_uri[uri] = game;
			games.add (game);

			if (game_added_callback != null)
				game_added_callback (game);

			return;
		}

		return_if_fail (medias.contains (media_id) == game_for_media_set.contains (media_set_id));

		// Check whether we already have a media and by extension a media set
		// and a game for this disc ID. If such a case, simply add the new URI.
		if (medias.contains (media_id)) {
			var media = medias.lookup (media_id);
			media.add_uri (uri);
			game_for_uri[uri] = game_for_media_set[media_set_id];

			try_add_game (game_for_uri[uri]);

			return;
		}

		// A game correspond to this URI but we don't have it yet: create it.

		var media_set = parser.create_media_set ();
		var game = create_game (parser, media_set);

		// Creating the Medias, MediaSet and Game worked, we can save them.

		media_set.foreach_media (media => {
			medias[media.id] = media;
		});

		game_for_uri[uri] = game;
		game_for_media_set[media_set_id] = game;
		games.add (game);

		try_add_game (game);
	}

	private void try_add_game (Game game) {
		if (game_added_callback == null)
			return;

		bool is_complete = true;
		game.media_set.foreach_media (media => {
			is_complete &= (media.get_uris ().length != 0);
		});

		if (!is_complete)
			return;

		game_added_callback (game);
	}

	public Game? query_game_for_uri (Uri uri) {
		if (game_for_uri.contains (uri))
			return game_for_uri[uri];

		return null;
	}

	public void foreach_game (GameCallback game_callback) {
		games.foreach ((game) => game_callback (game));
	}

	public void set_game_added_callback (GameCallback game_callback) {
		game_added_callback = game_callback;
	}

	private Game create_game (GameParser parser, MediaSet? media_set = null) throws Error {
		var uid = new Uid (parser.get_uid ());

		string title = null;
		if (media_set != null)
			title = media_set.title;
		else
			title = parser.get_title ();

		var media = new GriloMedia (title, platform.get_presentation_mime_type ());
		var cover = new CompositeCover ({
			new LocalCover (parser.uri),
			new GriloCover (media, uid)}
		);
		var icon = parser.get_icon ();

		var game = new Game (uid, parser.uri, title, platform);
		game.set_cover (cover);
		game.media_set = media_set;

		if (icon != null)
			game.set_icon (icon);

		return game;
	}
}
